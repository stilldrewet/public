<?php
include "db.php";
include "config.php";

if (isset($_GET['delete'])) {
    $all_id=$_GET['checkbox'];
    $extract_id=implode(',',$all_id);
    $query="DELETE FROM `products` WHERE `product_id` IN($extract_id)";
    $query_run=mysqli_query($connection,$query);
    $server=$config['db']['server'];
    header("Location: http://$server", true, 301);
    exit();
}
?>