<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Product list">
    <link rel="stylesheet" href="css/index.css"></link>
    <title>Product list</title>
</head>
<body>
    <form id="invisible" method="GET" action="includes/mass-delete.php">
    <div class="nav-bar">
        <div class="home">
            <a href="index.php"><h1>Product list</h1></a>            
        </div>
        <div class="buttons">
            <div class="left-btn"><a href="add-product.php"><button id="btn-add" type="button">ADD</button></a></div>
            <div class="right-btn"><button id="delete-product-btn" name="delete" type="submit">MASS DELETE</button></div>
        </div>
    </div>
    
        <div class="products">
            <?php
                include "includes/db.php";

                $products = mysqli_query($connection,"SELECT * FROM `products` ORDER BY `product_type_id` DESC");

                while ($record = mysqli_fetch_assoc($products)) {
                
                    $sign='';
                    $prefix='';
                    if ($record['product_type_id']=='1') {
                        $sign=' MB';
                        $prefix='Size: ';
                    }
                    else if ($record['product_type_id']=='2') {
                        $sign=' KG';
                        $prefix='Weight: ';
                    } else {
                        $sign='';
                        $prefix='Dimensions: ';
                    }
                    
                    echo (
                    '<div class="product-container">'.
                        
                        '<div class="checkbox">'.'<input type="checkbox" class="delete-checkbox"
                        name="checkbox[]" value="'.$record['product_id'].'">'.
                        '</div>'.

                        '<li>'.$record['sku'].'</li>'.
                        '<li>'.$record['name'].'</li>'.
                        '<li>'.$record['price'].' $'.'</li>'.
                        '<li>'.$prefix.$record['value']."$sign".'</li>'.
                    '</div>'
                );
                    
                    
                }
            ?>
        </div>
    </form>


    <footer>
        <hr>
        <p>Scandiweb Test assignment</p>
    </footer>

</body>
</html>