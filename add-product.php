<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Add product">
    <link rel="stylesheet" href="css/index.css"></link>
    <title>Product list</title>
</head>
<body>
    <div class="nav-bar">
        <div class="home">
            <a href="index.php"><h1>Product list</h1></a>            
        </div>

    </div>
<div class="form">
    <?php
        include "includes/db.php";

    ?>
    <form id="add" method="POST" action="includes/form-recorder.php">
    <div>
        <div class="form-rows">
            <label for="sku">SKU</label>
            <input required type="text" name="sku" id="sku" placeholder="SKU">
        </div>
        <br>
        <div class="form-rows"> 
            <label for="name">Name</label>
            <input required type="text" name="name" id="name" placeholder="Name">
        </div>
        <br>
        <div class="form-rows">
            <label for="price">Price</label>
            <input required type="number" step=".01" name="price" id="price" placeholder="Price">
        </div>
        <br>
        <div class="form-rows selecting">
            <label for="types">Type switcher:</label>
            <select id="productType" onchange="getSelectedValue()" name="product_type_id">
                <option value=""></option>
                <option value="1">DVD</option>
                <option value="2">Book</option>
                <option value="3">Furniture</option>
            </select>
            <script src="js/index.js"></script>
        </div>
        <br>
        <div class="hidden">
            <div id="size_input_field">
                <label for="size">Size (in MB)</label>
                <input type="number" step=".01" name="size" id="size" placeholder="Please, provide size">
            </div>
            <br>
            <div id="weight_input_field">
                <label for="weight">Weight (in KG)</label>
                <input type="number" step=".001" name="weight" id="weight" placeholder="Please, provide weight">
            </div>
            <br>
            <div id="dimensions_input_field">
                <label for="height">Height:</label>
                <input type="text" name="height" id="height" placeholder="Height"><br>
                <label for="width">Width:</label>
                <input type="text" name="width" id="width" placeholder="Width"><br>
                <label for="length">Length:</label>
                <input type="text" name="length" id="length" placeholder="Leight"><br>
            </div>
            
        </div>
    </div>

    <div>
        <input id="save" type="submit" value="Save"/>
        <input id="cancel" type="reset" value="Cancel"/>
    </div>



    </form>
</div>


    <footer>
        <hr>
        <p>Scandiweb Test assignment</p>
    </footer>
</body>
</html>