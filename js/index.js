function getSelectedValue() {
    var selectedValue = document.getElementById("productType").value;

    switch (selectedValue) {
        case '1':
            document.getElementById("size_input_field").style.display = "none";
            document.getElementById("weight_input_field").style.display = "none";
            document.getElementById("dimensions_input_field").style.display = "none";
            document.getElementById("size_input_field").style.display = "block";
            break;
        case '2':
            document.getElementById("size_input_field").style.display = "none";
            document.getElementById("weight_input_field").style.display = "none";
            document.getElementById("dimensions_input_field").style.display = "none";
            document.getElementById("weight_input_field").style.display = "block";
            break;
        case '3':
            document.getElementById("size_input_field").style.display = "none";
            document.getElementById("weight_input_field").style.display = "none";
            document.getElementById("dimensions_input_field").style.display = "none";
            document.getElementById("dimensions_input_field").style.display = "block";
        break;
        default:
            document.getElementById("size_input_field").style.display = "none";
            document.getElementById("weight_input_field").style.display = "none";
            document.getElementById("dimensions_input_field").style.display = "none";
    
            break;
    }
}
